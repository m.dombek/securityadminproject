var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser')
// const path = require('path');
var app = express();
var port = process.env.PORT || 3030 //80;
const url = process.env.API_URL || "http://localhost:3030" //"radiant-stream-68356.herokuapp.com";

console.log("URL IS: " + url)

app.use(express.static(`${__dirname}/client`));

app.use(cors({
    origin: "http://localhost:3000" // url + ":" +  port
}));

app.use(bodyParser.json());

var rows = [
    { id: 1, requestTime: '2020-05-14 12:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "AnnieEd@gmail.com", subject: 'Fwd: Greendale Community College', categorizedAs: "Phishing", status: "open" },
    { id: 2, requestTime: '2020-09-30 16:31', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "winger11@gmail.com", subject: 'Greendale Community College', categorizedAs: "Phishing", status: "accepted" },
    { id: 3, requestTime: '2020-09-29 12:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "abed@walla.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "open" },
    { id: 4, requestTime: '2020-05-27 14:30', requestedBy: 'b.perry@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Bank of America Account pending', categorizedAs: "Phishing", status: "rejected" },
    { id: 5, requestTime: '2020-05-27 11:54', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "abed@walla.com", subject: 'Fwd: Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 6, requestTime: '2019-05-26 12:44', requestedBy: 'joshd@gmail.com', recipient: 'Requester', sender: "abed@walla.com", subject: 'Fwd: Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 7, requestTime: '2019-09-25 17:23', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@walla.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "rejected" },
    { id: 8, requestTime: '2019-09-24 12:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 9, requestTime: '2019-04-23 12:52', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "rejected" },
    { id: 10, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 11, requestTime: '2019-03-28 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 12, requestTime: '2018-07-29 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 13, requestTime: '2018-06-01 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 14, requestTime: '2018-05-07 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "accepted" },
    { id: 15, requestTime: '2018-04-05 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Junk mail', categorizedAs: "Phishing", status: "open" },
    { id: 16, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "accepted" },
    { id: 17, requestTime: '2019-03-28 22:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "accepted" },
    { id: 18, requestTime: '2018-07-21 15:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "accepted" },
    { id: 19, requestTime: '2018-06-01 14:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },    
    { id: 20, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 21, requestTime: '2019-03-28 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 22, requestTime: '2018-07-29 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 23, requestTime: '2018-06-01 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 24, requestTime: '2018-05-07 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 25, requestTime: '2018-04-05 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Junk mail', categorizedAs: "Phishing", status: "open" },
    { id: 26, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 27, requestTime: '2019-03-28 22:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 28, requestTime: '2018-07-21 15:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 29, requestTime: '2018-06-01 14:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 30, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 31, requestTime: '2019-03-28 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "accepted" },
    { id: 32, requestTime: '2018-07-29 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "accepted" },
    { id: 33, requestTime: '2018-06-01 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 34, requestTime: '2018-05-07 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "rejected" },
    { id: 35, requestTime: '2018-04-05 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Junk mail', categorizedAs: "Phishing", status: "rejected" },
    { id: 36, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "rejected" },
    { id: 37, requestTime: '2019-03-28 22:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "rejected" },
    { id: 38, requestTime: '2018-07-21 15:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "accepted" },
    { id: 39, requestTime: '2018-06-01 14:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 40, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 41, requestTime: '2019-03-28 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 42, requestTime: '2018-07-29 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 43, requestTime: '2018-06-01 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 44, requestTime: '2018-05-07 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 45, requestTime: '2018-04-05 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Junk mail', categorizedAs: "Phishing", status: "open" },
    { id: 46, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 47, requestTime: '2019-03-28 22:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 48, requestTime: '2018-07-21 15:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "accepted" },
    { id: 49, requestTime: '2018-06-01 14:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 50, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 51, requestTime: '2019-03-28 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 52, requestTime: '2018-07-29 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 53, requestTime: '2018-06-01 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 54, requestTime: '2018-05-07 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 55, requestTime: '2018-04-05 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Junk mail', categorizedAs: "Phishing", status: "open" },
    { id: 56, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 57, requestTime: '2019-03-28 22:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 58, requestTime: '2018-07-21 15:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 59, requestTime: '2018-06-01 14:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 60, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 61, requestTime: '2019-03-28 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "accepted" },
    { id: 62, requestTime: '2018-07-29 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 63, requestTime: '2018-06-01 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 64, requestTime: '2018-05-07 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 65, requestTime: '2018-04-05 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Junk mail', categorizedAs: "Phishing", status: "open" },
    { id: 66, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 67, requestTime: '2019-03-28 22:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 68, requestTime: '2018-07-21 15:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 69, requestTime: '2018-06-01 14:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 70, requestTime: '2018-05-09 13:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },    
    { id: 71, requestTime: '2019-03-28 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 72, requestTime: '2018-07-29 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 73, requestTime: '2018-06-01 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 74, requestTime: '2018-05-07 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 75, requestTime: '2018-04-05 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Junk mail', categorizedAs: "Phishing", status: "open" },
    { id: 76, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 77, requestTime: '2019-03-28 22:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 78, requestTime: '2018-07-21 15:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 79, requestTime: '2018-06-01 14:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },    
    { id: 80, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 81, requestTime: '2019-03-28 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 82, requestTime: '2018-07-29 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 83, requestTime: '2018-06-01 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 84, requestTime: '2018-05-07 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 85, requestTime: '2018-04-05 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Junk mail', categorizedAs: "Phishing", status: "open" },
    { id: 86, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 87, requestTime: '2019-03-28 22:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 88, requestTime: '2018-07-21 15:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 89, requestTime: '2018-06-01 14:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 90, requestTime: '2018-05-09 13:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },    
    { id: 91, requestTime: '2019-03-28 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 92, requestTime: '2018-07-29 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 93, requestTime: '2018-06-01 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 94, requestTime: '2018-05-07 22:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 95, requestTime: '2018-04-05 22:34', requestedBy: 'janedoe@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Junk mail', categorizedAs: "Phishing", status: "open" },
    { id: 96, requestTime: '2019-04-22 18:30', requestedBy: 'winger11@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Bank of America Account pending', categorizedAs: "Phishing", status: "open" },
    { id: 97, requestTime: '2019-03-28 22:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'Fwd: You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
    { id: 98, requestTime: '2018-07-21 15:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "abed@gmail.com", subject: 'Check out this attachment', categorizedAs: "Phishing", status: "open" },
    { id: 99, requestTime: '2018-06-01 14:34', requestedBy: 'johndoe@gmail.com', recipient: 'Requester', sender: "hawthorne@gmail.com", subject: 'Fwd: Nigerian Prince', categorizedAs: "Phishing", status: "rejected" },
    { id: 100, requestTime: '2018-05-09 13:34', requestedBy: 'AnnieEd@gmail.com', recipient: 'Requester', sender: "notpierce@gmail.com", subject: 'You WON a free vacation!', categorizedAs: "Phishing", status: "open" },
]

app.get('/', (req, res) => { 
    res.send(rows);
})

app.get('/:status', (req, res) => {
    var filteredRows = rows.filter(function(currRow) {
        return currRow.status.localeCompare(req.params.status) == 0;
    })

    res.send(filteredRows);
})

app.post('/reject', (req, res) => {
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].id == req.body.id) {
            rows[i].status = "rejected";
            break;
        }
    }

    res.send("Success");
})

app.post('/rejectMultipleRows', (req, res) => {
    for (var i = 0; i < rows.length; i++) {
        if (req.body.ids.includes(rows[i].id)) {
            rows[i].status = "rejected";
        }
    }

    res.send("Success");
})

app.post('/filter', (req, res) => {
    var filteredRows = [];

    if (req.body.id == "") {
        var filteredRows = rows.filter(function(currRow) {
            return (currRow.status.localeCompare(req.body.status) == 0);
        });
    } else {
        var filteredRows = rows.filter(function(currRow) {
            return (currRow.status.localeCompare(req.body.status) == 0 && currRow.id.toString().includes(req.body.id));
        });
    }

    res.send(filteredRows);
})

// Handles any requests that don't match the ones above
app.get('*', (req,res) =>{
    res.sendFile(`${__dirname}/client/index.html`);
});

app.listen(port, () => console.log('Server listening on port ' + port));