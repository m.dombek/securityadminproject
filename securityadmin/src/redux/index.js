import { combineReducers, createStore } from 'redux';
import tableReducer from './mainTable/reducer'

const rootReducer = combineReducers({
    tableReducer
});

const store = createStore(rootReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export { store }