import { 
    SET_ROWS, 
    CLICK_ROW, 
    CHANGE_STATUS_FILTER, 
    REJECT_ROW, 
    REJECT_MULTIPLE_ROWS, 
    CHECK_ROW,
    CHECK_ALL_ROWS } from './actionTypes';

const initialState = {
    selectedRowIndex: undefined,
    selectedRowId: undefined,
    isAllRowsChecked: false,
    checkedRows: [],
    statusFilter: 'open',
    rows: []
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SET_ROWS:
            return {
                ...state,
                rows: action.rows
            }
        case CLICK_ROW:
            return { 
                ...state, 
                selectedRowId: state.rows[action.index].id,
                selectedRowIndex: state.selectedRowIndex == action.index ?
                    undefined : action.index
             }
        case REJECT_ROW: {
            const newRows = [...state.rows].filter(currRow => {
                return currRow.id != action.id
            });

            return {
                ...state,
                rows: newRows,
                isAllRowsChecked: false,
                selectedRowId: undefined,
                selectedRowIndex: undefined
            }
        }
        case REJECT_MULTIPLE_ROWS: {
            const newRows = [...state.rows].filter(currRow => {
                return !action.ids.includes(currRow.id)
            });

            return {
                ...state,
                rows: newRows,
                checkedRows: [],
                isAllRowsChecked: false,
                selectedRowId: undefined,
                selectedRowIndex: undefined
            }
        }
        case CHANGE_STATUS_FILTER:
            return {
                ...state,
                statusFilter: action.statusFilter
            }
        case CHECK_ROW:
            var m = state.checkedRows.slice();
            m.splice(m.indexOf(state.rows[action.index].id), 1);

            return {
                ...state,
                checkedRows: (state.checkedRows.includes(state.rows[action.index].id)) ?
                    m : [...state.checkedRows, state.rows[action.index].id]

            }
        case CHECK_ALL_ROWS:
            return {
                ...state,
                isAllRowsChecked: !(state.checkedRows.length == state.rows.length),
                checkedRows: (state.checkedRows.length == state.rows.length) ?
                    [] : state.rows.map(currRow => currRow.id)
            }
        default:
            return state;
    }
}