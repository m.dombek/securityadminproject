import { 
    DELETE_ROW, 
    CLICK_ROW, 
    CHANGE_STATUS_FILTER, 
    REJECT_ROW, 
    REJECT_MULTIPLE_ROWS, 
    SET_ROWS, 
    CHECK_ROW,
    CHECK_ALL_ROWS } from './actionTypes'

export const setRows = rows => ({
    type: SET_ROWS,
    rows
})

export const deleteRow = index => ({
    type: DELETE_ROW,
    index
})

export const clickRow = index => ({
    type: CLICK_ROW,
    index
})

export const changeStatusFilter = statusFilter => ({
    type: CHANGE_STATUS_FILTER,
    statusFilter
})

export const rejectRow = id => ({
    type: REJECT_ROW,
    id
})

export const rejectMultipleRows = ids => ({
    type: REJECT_MULTIPLE_ROWS,
    ids
})

export const checkRow = index => ({
    type: CHECK_ROW,
    index
})

export const checkAllRows = () => ({
    type: CHECK_ALL_ROWS
})