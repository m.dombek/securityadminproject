export const SET_ROWS = "SET_ROWS"
export const DELETE_ROW = "DELETE_ROW"
export const CLICK_ROW = "CLICK_ROW"
export const CHANGE_STATUS_FILTER = "CHANGE_STATUS_FILTER"
export const REJECT_ROW = "REJECT_ROW"
export const REJECT_MULTIPLE_ROWS = "REJECT_MULTIPLE_ROWS"
export const CHECK_ROW = "CHECK_ROW"
export const CHECK_ALL_ROWS = "CHECK_ALL_ROWS"