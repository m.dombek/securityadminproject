import React, { useCallback } from 'react';
import Row from '../Row/Row';
import { useSelector, useDispatch } from 'react-redux';
import { checkAllRows } from '../../redux/mainTable/actionCreators'

import './Table.css'

export default function Table({ rows, onRowClick, onRowCheck }) {     
    const dispatch = useDispatch();
    const selectedRowId = useSelector(state => state.tableReducer.selectedRowId);
    const checkedRows = useSelector(state => state.tableReducer.checkedRows);
    const isAllRowsChecked = useSelector(state => state.tableReducer.isAllRowsChecked);

    const handleCheckAll = useCallback(
        () => dispatch(checkAllRows()),
        [dispatch]
    )
      
    return (
        <table className="table">
            <tr className="table-header">
                <th>
                    <input type="checkbox" checked={isAllRowsChecked} className="table-cell-checkbox" onClick={handleCheckAll}></input>
                </th>
                <th>ID</th>
                <th>Request Time</th>
                <th>Requested By</th>
                <th>Recipient</th>
                <th>Sender</th>
                <th>Subject</th>
                <th>Categorized As</th>
                <th>Status</th>
            </tr>
            {   rows &&   
                rows.map((currRowData, index) => {
                    return (
                        <Row index={index} data={currRowData} isSelected={selectedRowId == currRowData.id} onClick={onRowClick} isChecked={checkedRows.includes(currRowData.id)} onCheck={onRowCheck}></Row>
                    )
                }) 
            }
        </table>
    )
}