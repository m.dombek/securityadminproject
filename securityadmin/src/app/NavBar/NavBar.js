import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import './NavBar.css';

import Button from '@material-ui/core/Button';

class NavBar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="nav-bar">
                <div className="divider"></div>
                <br/>
                <div className="button-container">
                    {
                        this.props && this.props.options &&
                        this.props.options.map((currOption, index) => {
                            return (
                                // <Button size="small" className="nav-bar-button">
                                    <Link to={ currOption.url } className="nav-bar-link">{ currOption.title }</Link>
                                // </Button>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default NavBar;