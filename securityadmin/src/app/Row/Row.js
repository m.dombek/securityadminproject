import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';

import './Row.css'

export default function Row({ index, data, isSelected, onClick, isChecked, onCheck }) {
    const handleClick = useCallback(() => onClick(index), [onClick, index]);
    const handleCheck = useCallback(() => onCheck(index), [onCheck, index]);

    return (
        <tr className={ isSelected ? "selected-table-row" : "table-row" } onClick={handleClick}>               
            <td className="table-cell">
                <input type="checkbox" checked={isChecked} onClick={handleCheck} className="table-cell-checkbox"></input>
            </td>
            <td className="table-cell">{data.id}</td>
            <td className="table-cell">{data.requestTime}</td>
            <td className="table-cell">{data.requestedBy}</td>
            <td className="table-cell">{data.recipient}</td>
            <td className="table-cell">{data.sender}</td>
            <td className="table-cell">{data.subject}</td>
            <td className="table-cell">{data.categorizedAs}</td>
            <td className="table-cell">{data.status}</td>
        </tr>
    )
}