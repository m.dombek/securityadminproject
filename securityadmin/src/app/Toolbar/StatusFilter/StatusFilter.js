import React from 'react';

export default function StatusFilter({ onChange }) {
    const handleChange = () => {
        var e = document.getElementById("status");
        var statusFilter = e.options[e.selectedIndex].value;
        onChange(statusFilter);
    }

    return (   
        <div>                        
            <label for="status">Status </label>
            <select id="status" className="status-drop-down-list" onChange={handleChange}>
                <option value="open">Open</option>
                <option value="rejected">Rejected</option>
                <option value="accepted">Accepted</option>
            </select>
        </div>
    )
}