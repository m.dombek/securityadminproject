import React, { useCallback } from 'react';

import Button from '@material-ui/core/Button';
import RefreshIcon from '@material-ui/icons/Refresh';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import StatusFilter from './StatusFilter/StatusFilter';
import AssignmentReturnedIcon from '@material-ui/icons/AssignmentReturned';
import classNames from 'classnames';
import { useSelector, useDispatch } from 'react-redux';
import { setRows, changeStatusFilter, rejectRow, rejectMultipleRows } from '../../redux/mainTable/actionCreators';

import './Toolbar.css';

export default function Toolbar() {
    const dispatch = useDispatch();
    const selectedRowId = useSelector(state => state.tableReducer.selectedRowId);
    const checkedRows = useSelector(state => state.tableReducer.checkedRows);
    const statusFilter = useSelector(state => state.tableReducer.statusFilter);
    const port = process.env.PORT || 80;
    const url = "http://localhost:3030" //"https://radiant-stream-68356.herokuapp.com"
    
    const handleRejectClick = () => {
        if (checkedRows.length > 0) {
            fetch(url + '/rejectMultipleRows', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    ids: checkedRows
                })
            }).then(() => dispatch(rejectMultipleRows(checkedRows)));
        } else if (selectedRowId) {
            fetch(url + '/reject', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    id: selectedRowId
                })
            }).then(() => dispatch(rejectRow(selectedRowId)));
        }
    }

    const handleRefreshClick = () => {
        fetch(url + "/" + statusFilter)
            .then(response => response.json())
            .then(data => dispatch(setRows(data)),
            [dispatch]);
    }

    const handleStatusFilterChange = useCallback(
        (statusFilter) => dispatch(changeStatusFilter(statusFilter)), [dispatch]
    )

    const handleSearch = () => {
        var statusElement = document.getElementById("status");
        var statusFilter = statusElement.options[statusElement.selectedIndex].value;

        var searchElement = document.getElementById("searchBox");
        var searchFilter = searchElement.value;
        
        fetch(url + '/filter', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                status: statusFilter,
                id: searchFilter
            })
        })
        .then(response => response.json())
        .then(data => dispatch(setRows(data)));
    }

    return (
        <div className="toolbar-container">
            <div className="toolbar-feature">
                <Button size="small" color="primary" variant="contained" onClick={handleSearch}>
                    <SearchIcon></SearchIcon>
                </Button>
            </div>
            
            <input id="searchBox" type="text" placeholder="Search" className={classNames('toolbar-feature', 'search-box')} />
            <div className="toolbar-feature">                    
                <Button size="small" onClick={ handleRefreshClick }>
                    <RefreshIcon></RefreshIcon>
                    Refresh
                </Button>
            </div>
            <div className="toolbar-feature">                    
                <Button size="small" onClick={ handleRejectClick }>
                    <ClearIcon></ClearIcon>
                    Reject
                </Button>
            </div>
            <div className="toolbar-feature">                    
                <Button size="small">
                    <AssignmentReturnedIcon></AssignmentReturnedIcon>
                    Release
                </Button>
            </div>
            <div>

            <StatusFilter onChange={handleStatusFilterChange}></StatusFilter>

            </div>
        </div>
    )
}