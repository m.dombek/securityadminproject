import React from 'react';
import { useSelector } from 'react-redux';

import './Details.css'

export default function Details() {
    const selectedRowIndex = useSelector(state => state.tableReducer.selectedRowIndex);
    const selectedRowData = useSelector(state => state.tableReducer.rows[selectedRowIndex]);

    return (
        selectedRowData != undefined && <div className="details-container">
            <div className="details-title">
                Details
            </div>
            <div className="details-data-container">
                <div className="email-information-container">
                    <table>
                        <th className={"details-information-title"}>
                            <td>E-Mail Information</td>
                        </th>
                        <tr>
                            <td>ID:</td>
                            <td>{selectedRowData.id}</td>
                        </tr>
                        <tr>
                            <td>Request Time:</td>
                            <td>{selectedRowData.requestTime}</td>
                        </tr>
                        <tr>
                            <td>Recipient:</td>
                            <td>{selectedRowData.recipient}</td>
                        </tr>
                        <tr>
                            <td>Subject:</td>
                            <td>{selectedRowData.subject}</td>
                        </tr>
                        <tr>
                            <td>Sender:</td>
                            <td>{selectedRowData.sender}</td>
                        </tr>
                        <tr>
                            <td>Categorized As:</td>
                            <td>{selectedRowData.categorizedAs}</td>
                        </tr>
                    </table>            
                </div>
                <div className="request-information-container">
                <table>
                        <th className={"details-information-title"}>
                            <td>Request Information</td>
                        </th>
                        <tr>
                            <td>Request Time:</td>
                            <td>{selectedRowData.requestTime}</td>
                        </tr>
                        <tr>
                            <td>Requested By:</td>
                            <td>{selectedRowData.requestedBy}</td>
                        </tr>
                        <tr>
                            <td>Justification:</td>
                            <td>{selectedRowData.justification}</td>
                        </tr>
                    </table>  
                </div>
            </div>
        </div>
    )
}