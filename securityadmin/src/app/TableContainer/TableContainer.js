import React, { useCallback, useEffect } from 'react'

import Table from '../Table/Table'
import Toolbar from '../Toolbar/Toolbar'
import Details from '../Details/Details'
import { useSelector, useDispatch } from 'react-redux'
import { setRows, clickRow, checkRow } from '../../redux/mainTable/actionCreators'

import './TableContainer.css'

export default function TableContainer() {
    const dispatch = useDispatch()

    const statusFilter = useSelector(state => state.tableReducer.statusFilter);

    const port = process.env.PORT || 80;
    const url = "http://localhost:3030" //"https://radiant-stream-68356.herokuapp.com"
    
    useEffect(() => {
        fetch(url + "/" + statusFilter)
        .then(response => response.json())
        .then(data => dispatch(setRows(data)),
        [dispatch]);
    }, [statusFilter])

    const rows = useSelector(state => state.tableReducer.rows)

    const handleRowClick = useCallback(
        (index) => dispatch(clickRow(index)),
        [dispatch]
    )

    const handleRowCheck = useCallback(
        (index) => dispatch(checkRow(index)),
        [dispatch]
    )

    return (
        <div className="table-container">
            <Toolbar></Toolbar>     
            <div className="scrollable-table-container">                  
                <Table rows={rows} onRowClick={handleRowClick} onRowCheck={handleRowCheck}></Table>
            </div>     
            <Details></Details>
        </div>
    )
}