import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import TableContainer from './TableContainer/TableContainer';
import NavBar from './NavBar/NavBar';

import './App.css'

function App() {
  const navBarOptions = [
    { id: 1, url: '/', title: 'Release Requests' },
    { id: 2, url: '/quarantined-emails', title: 'All Quarantined E-Mails' }
  ]  

  return (
    <div className="app-container">
      <Router>
        <NavBar options={navBarOptions}></NavBar>
        <Switch>          
          <Route path="/quarantined-emails">
            <div></div>
          </Route>
          <Route path="/">            
            <TableContainer />
          </Route>
        </Switch> 
      </Router>   
         
    </div>
  );
}

export default App;
